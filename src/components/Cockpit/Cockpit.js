import React from 'react';
//import classes from './Cockpit.css';
import classes from './Cockpit.css';
import logo from '../../assets/dart_menu.png';

const cockpit =(props)=>{
    return(
        <div>
            <img className={classes.logo} src = {logo} alt = 'Main logo'></img>
            <h2 className={classes.title}>GIAP's Dart</h2>
            <div>
            <button className={classes.nav_button} onClick={props.addPlayer}>{props.buttonText}</button>
            <button className={classes.nav_button} onClick={props.removePlayer}>Remove player</button>
            <button className={classes.nav_button} onClick={props.runSettings}>Settings</button>
            <button className={classes.nav_button} onClick={props.runHow}>How it works?</button>
            </div> 
        </div>
    );
}

export default cockpit