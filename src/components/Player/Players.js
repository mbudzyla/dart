import React from 'react';
import Player from './Player';

const Players = (props) => props.players.map((player, index) => {
        return <Player
          inputChangeHandler = {props.inputChangeHandler}
          deleteHandler = {() => props.deleteHandler(index)}
          saveScoreHandler = {() => props.saveScoreHandler(index)}
          name = {player.name} 
          score = {player.score}
          key = {player.id}
          scheduleCounter = {props.scheduleCounter}
          scheduleManager = {props.scheduleManager}
          index = {index}
          lastScore = {player.lastScore}
          avg = {player.scoreAvg}
          >       
          </Player>
          
});
export default Players;