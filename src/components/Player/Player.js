import React, { Component} from 'react';
import "react-table/react-table.css";
import playerimg from '../../assets/person.png';
// import classes from './Player.css'
import classes from './Player.css'

class Player extends Component{
 
  componentDidMount(){
      if (this.props.index === 0){
        this.props.scheduleManager()
      }
  }
  render () {
    return(
      <div className = {classes.PlayerContainer}>
        <div className = {classes.IconHolder}>
          <img className = {classes.playerIcon} src = {playerimg} alt = 'PlayerImg' onClick= {this.props.deleteHandler}></img>
          
          <div className={classes.nameHolder}>
            {this.props.name}
          </div> 

        </div>
          <div className = {classes.scores}>
            <p className = {classes.stats}>Last score: {this.props.lastScore}</p>
            <p className = {classes.stats}>Average: {this.props.avg}</p>
          </div>

          <div className = {classes.Points}>
            Points left: {this.props.score}
          </div>
  
          <div className = {classes.test}>
            <button name = "scoreBtn" className = {classes.saveScoreBtn} onClick = {this.props.saveScoreHandler}>Save score</button>
          </div>
       
        
       </div>
  )

  }
  

}

export default Player;