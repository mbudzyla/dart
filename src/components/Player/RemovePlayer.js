import React from 'react';
import classes from './../General.css';
import playerimg from '../../assets/person.png';
import style from './RemovePlayer.css'
const RemovePlayer = (props) => {
    return(
        <div className = {classes.TextCenter}> 
            <p className ={classes.whiteP}>How to remove player?</p>
            <div className = {style.iconDiv} >
                <img className = {classes.playerIcon} src = {playerimg} alt = 'PlayerImg'></img>
            </div>
            <p className = {classes.whiteP}>Click player's icon to remove</p>
        </div>
    )
}
export default RemovePlayer
