import React from 'react';
import classes from './AddPlayer.css'
import generalCSS from './../General.css'
const AddPlayer = (props) => {
    return(
        <div> 
            <p className = {generalCSS.whiteP}>Here you can add new player!</p>
            <input className = {classes.dashedInput} onChange= {(event) => props.nameChangedHandler(event,'tmpName')} type="text" placeholder= "Enter your name"></input>
            <div className = {classes.addBtnDiv}>
            <button className = {generalCSS.coralBtn} onClick = {(event) => props.addingAction(event)}> Add</button>   
            </div>
            
        </div>
    )
}
export default AddPlayer