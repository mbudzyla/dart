import React, { Component } from 'react';
import classes from './App.css';
import Cockpit from '../Cockpit/Cockpit.js';
import AddPlayer from '../Player/AddPlayer'
import Players from '../Player/Players'
import Settings from '../Settings//Settings.js'
import RemovePlayer from '../Player/RemovePlayer.js'
import Navbar from '../Navbar/Navbar.js'
import HowItWorks from '../How/how.js';

class App extends Component {
  
  state  = {
    players: [],
    showPlayer: false,
    showSettings: false,
    showRemove: false,
    showHow: false,
    tmpName: '',
    addButtonText: 'Add Player',
    maxPoints: 501,
    firstInput: 0,
    secondInput: 0,
    thridInput: 0,
    isCockpitVisible: true,
    schedule: 0,
    round: 1 
    
  }
  
  inputChangeHandler = (event, inputName) => {
    this.setState({[inputName]:event.target.value})
  }

  removePlayerAction = () => {
    const showRemove = this.state.showRemove;
    this.setState({showRemove: !showRemove});
    
    const addShow = this.state.showPlayer;
    if (addShow){
      this.setState({showPlayer: !addShow});
    }

    const showSettings = this.state.showSettings;
    if (showSettings){
      this.setState({showSettings: !showSettings});   
    }
    const showHow = this.state.showHow;
    if (showHow){
      this.setState({showHow: !showHow})
    }
  }

  deletePlayerHandler = (playerIndex) => {
    const players = [...this.state.players];
    players.splice(playerIndex, 1);
    this.setState({players: players})
    this.playersChecker()
  }

  settingsHandler = () => {
    const doesShow = this.state.showSettings;
    this.setState({showSettings: !doesShow});
    
    const addShow = this.state.showPlayer;
    if (addShow){
      this.setState({showPlayer: !addShow});
    }
    const showRemove = this.state.showRemove;
    if(showRemove){
      this.setState({showRemove: !showRemove});
    }
    const showHow = this.state.showHow;
    if (showHow){
      this.setState({showHow: !showHow})
    }
  }
    
  addPlayerHandler = () => {
    const doesShow = this.state.showPlayer;
    this.setState({showPlayer: !doesShow});
    
    const settingsShow = this.state.showSettings;
    if (settingsShow){
      this.setState({showSettings: !settingsShow});
    }
    const showRemove = this.state.showRemove;
    if(showRemove){
      this.setState({showRemove: !showRemove});
    }
    const showHow = this.state.showHow;
    if (showHow){
      this.setState({showHow: !showHow})
    }
  }

  addPlayerAction = (event) => {
    let unique_id = new Date().valueOf();
    const players = [...this.state.players];
    let inputScore = this.state.maxPoints;
    players.push({id:unique_id,
      name: this.state.tmpName,
      score: inputScore,
      lastScore: 0,
      scoreAvg: 0
    })
    this.setState({players:players})
    this.setState({isCockpitVisible:false})
    if (this.state.players.length === 1){
      this.setState({schedule: 1})
    }
  }

  howAction = () => {
    const showHow = this.state.showHow;
    this.setState({showHow: !showHow});
    
    const addShow = this.state.showPlayer;
    if (addShow){
      this.setState({showPlayer: !addShow});
    }

    const showSettings = this.state.showSettings;
    if (showSettings){
      this.setState({showSettings: !showSettings});   
    }
    const showRemove = this.state.showRemove;
    if(showRemove){
      this.setState({showRemove: !showRemove});
    }
  }
  
  playersChecker = () => {
    if(this.state.players.length > 1){
      this.setState({isCockpitVisible:false});
    }
    else{
      this.setState({isCockpitVisible:true});
      this.setState({schedule: 0})
    }
  }

  saveScoreHandler = (playerIndex) => {
    const players = [...this.state.players];
    let hits = +this.state.firstInput + +this.state.secondInput + +this.state.thridInput;
    let points = players[playerIndex].score;
    let score = null;
    let avg = null;
    if (hits <= points){
      score = points - hits
      avg = this.scoreAverage(score)
      players[playerIndex].scoreAvg = avg
      players[playerIndex].score = score
      players[playerIndex].lastScore = hits
    } 
    this.setState({players:players})
    this.clearStateInputs()
    this.clearInputs()
    this.scheduleCounter()
    this.scheduleManager()
  }

  scoreAverage = (playersScore)  => {
    let  avg = (this.state.maxPoints -  playersScore) / this.state.round

    return Math.round(avg)
  }

  clearStateInputs = () => {
    this.setState({firstInput:0})
    this.setState({secondInput:0})
    this.setState({thridInput:0})
  }

  clearInputs = () => {
    const inputs =  document.getElementsByTagName('input')
    for ( let i = 0; i < inputs.length; i++ ){
      inputs.item(i).value = ""
    }
  }

  scheduleCounter = () => {
    let schedule = this.state.schedule
    let howManyPlayers = this.state.players.length
    let round = this.state.round
    if (howManyPlayers > 1){
      schedule++
      if (schedule >= howManyPlayers){
        schedule = 0
        round++
           
      } 
    }
    this.setState({schedule: schedule}) 
    this.setState({round:round})
  }

  scheduleManager = () => {
    let buttons = document.getElementsByName('scoreBtn') 
    if (buttons.length > 0){
       if (this.state.schedule > 0){
          let index = this.state.schedule - 1
          buttons.item(index).style.display = "none"
        }
        if(buttons.length === 1){
          this.setState({schedule: 0})
        }
      }
    if (buttons.length > 1){
      if(this.state.schedule === 0 ){
        buttons.item(buttons.length - 1).style.display = "none"
        
      }
    }
    buttons.item(this.state.schedule).style.display = "inline-block"
  
    
  }

  render() {
    let players_comp = null;
    let addplayer = null;
    let settings = null;
    let removePlayer = null;
    let cockpit = null;
    let how = null;
    
    if(this.state.isCockpitVisible){
      cockpit = 
        <div className = {classes.cockpit}>
          <Cockpit
              addPlayer = {this.addPlayerHandler}
              removePlayer = {this.removePlayerAction}
              buttonText = {this.state.addButtonText}
              runSettings = {this.settingsHandler} 
              runHow = {this.howAction}
              GIAP DART>
          </Cockpit>
        </div>
    }else{
      cockpit = 
      <Navbar
        addPlayer = {this.addPlayerHandler}
        removePlayer = {this.removePlayerAction}
        runSettings = {this.settingsHandler} 
        runHow ={this.howAction}>
      </Navbar>
    }

    if (this.state.showPlayer){
      addplayer = 
      <AddPlayer 
        addingAction = {this.addPlayerAction}
        nameChangedHandler = {this.inputChangeHandler}
      ></AddPlayer> 
    }

    if(this.state.players.length > 0){
      players_comp = 
      <div className = {classes.playerDiv}>
        <h2 className = {classes.PlayersTitle}>Players:</h2>
          <div className={classes.InputsContainer}>
              <input className = {classes.inputScore} onChange = {(event) => this.inputChangeHandler(event,'firstInput')} type="number" min="0" max="180"></input>
              <input className = {classes.inputScore} onChange = {(event) => this.inputChangeHandler(event,'secondInput')} type="number" min="0" max="180"></input>
              <input className = {classes.inputScore} onChange = {(event) => this.inputChangeHandler(event,'thridInput')} type="number" min="0" max="180"></input>
          </div>
        <Players
          scheduleCounter = {this.scheduleCounter}
          scheduleManager = {this.scheduleManager}      
          inputChangeHandler = {this.inputChangeHandler}
          deleteHandler = {this.deletePlayerHandler}
          players = {this.state.players}
          saveScoreHandler = {this.saveScoreHandler}> 
        </Players>
      </div>
      
    }
    if(this.state.showHow){
      how = <HowItWorks></HowItWorks>
    }
    if(this.state.showSettings){
      settings = 
      <Settings
        pointsChangedHandler = {this.inputChangeHandler}>       
      </Settings>
    }
    if (this.state.showRemove){
      removePlayer = <RemovePlayer></RemovePlayer>
    }
 
    return (
        <div className = {classes.Main}>
            {cockpit}
            {settings}
            {addplayer}
            {removePlayer}
            {how}
            {players_comp}
            
        </div>
      )
    }
}

export default App;
