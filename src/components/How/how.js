import React from 'react';
import classes from './../General.css';
const HowItWorks = (props) => {
    return(
        <div className = {classes.how2}> 
            <h1 className ={classes.whiteP}>How it works?</h1>
            <h3 className ={classes.whiteP}>Add player</h3>
            <h3 className ={classes.whiteP}>Enter the results</h3>
            <h3 className ={classes.whiteP}>Click 'Save score'</h3>
            <h1 className ={classes.whiteP}>Enjoy!</h1>            
        </div>
    )
}
export default HowItWorks
