import React, { Component } from  'react';
import classes from './Navbar.css';
import logo from '../../assets/dart_menu.png';

class Navbar extends Component{
    constructor(props){
        super(props);
        this.state = {}
        this.handleScroll = this.handleScroll.bind(this)
        ;
    }

    handleScroll = () =>{
        this.setState({scroll:window.scrollY});
    }
    componentDidMount(){
        const navbar = document.querySelector('nav');
        this.setState({top:navbar.offsetTop, height: navbar.offsetHeight});
        window.addEventListener('scroll', this.handleScroll);
    }
    componentDidUpdate (){
        this.state.scroll > this.state.top ? document.body.style.paddingTop = [this.state.height] +'px':
        document.body.style.paddingTop = 0;
    }
    
    render (){
        return <nav className = {this.state.scroll  > this.state.top ?  classes.fixedNav: "" }>
            <ul>    
                <li onClick = {this.props.addPlayer}>Add Player</li>
                <li onClick = {this.props.removePlayer}>Remove Player</li>
                <li className = {classes.logoLi}><img className={classes.logo}src = {logo} alt = 'Main logo'></img> GIAP's Dart</li>
                <li onClick = {this.props.runSettings}>Settings</li>
                <li onClick = {this.props.runHow}>How it works?</li>
            </ul>
        </nav>
    };
}

export default Navbar;