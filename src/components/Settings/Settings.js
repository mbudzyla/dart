import React from 'react';
import classes from './../General.css';

const Settings = (props) => {
    return(
        <div className = {classes.TextCenter}> 
            <p className = {classes.whiteP}>Here you can set the maxiumim amount of points</p>
            <input onChange={(event) => props.pointsChangedHandler(event,'maxPoints')} type="number" placeholder = "Enter score"></input>
            <p className = {classes.textLooksLikeBtn}>Changes will be saved automatically</p> 
        </div>
    )
}
export default Settings
